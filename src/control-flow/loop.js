/**
 * pętla for
 */
console.warn('for');
for (var i = 0; i < 2; i++) {
    console.log('iteration', i);
}


/**
 * pętla for in
 */
console.warn('for in');
var person = {
    name: "Joe",
    phone: 35939393
};
for (var key in person) {
    console.log(key, person[key]);
}


/**
 * pętla while
 */
console.warn('while');
var countWhile = 2;
while (countWhile > 0) {
    console.log(countWhile);
    countWhile--;
}


/**
 * pętla do while
 */
console.warn('do while');
var countDoWhile = 0;
do {
    console.log(countDoWhile);
    countDoWhile++
} while (countDoWhile < 2)


/**
 * break example
 */
console.warn('for break');
var log = ["Good", "Good", "Error", "Good", "Good"];

for (var k = 0; i < log.length; k++) {
    if (log[k] === "Good") {
        console.log("System running ok");
    } else {
        console.log("OMG there's an error!");
        break;
    }
}


/**
 * continue example
 */
console.warn('for continue');
for (var j = 0; j < log.length; j++) {
    if (log[j] === "Error") {
        continue;
    }
    console.log("System running ok");
}