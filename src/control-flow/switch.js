/**
 * switch
 */
console.warn('switch');
var num = 1;
switch (num) {
    case 0:
        console.log("num == 0");
        break;
    case 1:
        console.log("num == 1");
        break;
    default:
        console.log("num - isn't 0 either 1");
        break;
}


/**
 * switch with true
 */
console.warn('switch with true');
var foo = 2;
switch (true) {
    case /[0-9]/.test(foo):
        console.log('foo is number');
        break;
    case /[a-z]/.test(foo):
        console.log('foo is string');
        break;
    default:
        console.log("foo isn't number either string");
        break;
}
