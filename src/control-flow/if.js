/**
 * example 1
 */
console.warn('example 1');
var condition = true;
if (condition) {
    console.log(condition);
} else {
    console.log(condition);
}


/**
 * example 2
 */
console.warn('example 2');
var num1 = 10;
var num2 = 5;
var result;

if (num1 > num2) {
    result = num1 + ' > ' + num2;
} else {
    result = num1 + ' <= ' + num2;
}
console.log(result);


/**
 * example 3
 * to samo tylko w skróconej wersji z wykorzystaniem operatora "?"
 */
console.warn('example 2');
result = num1 > num2
    ? num1 + ' > ' + num2
    : num1 + ' <= ' + num2;
console.log(result);