/**
 * przykład 1: zmienne przekazywane są do metody przez wartość
 */
console.warn('przekazywanie zmiennych przez referencje i wartość');
var x = 4;
function myFunction(x) {
    x = 5;
}

console.warn('pass by value');
console.log(x);
myFunction(x);
console.log(x);


/**
 * przykład 2: obiekty przekazywanie są jako referencje
 */
console.warn('pass by reference');
var obj = {value: 4};
function foo(passedObj) {
    passedObj.value = 5;
}

console.log(obj.value);
foo(obj);
console.log(obj.value);