/**
 * funkcja bez parametru
 */
console.warn('funkcja bez parametru');
function hello() {
    console.log('Witaj');
}
hello();


/**
 * funkcja z parametrem
 * @param name
 */
console.warn('funkcja z parametrem');
function helloWithName(name) {
    console.log('Witaj ' + name); // Witam John
}
helloWithName("John");
helloWithName("Mike");


/**
 * funkcja automatyczna i anonimowa zarazem
 */
console.warn('funkcja automatyczna');
(function (param) {
    console.log('funkcja ' + param);
})('auto');


/**
 * referencja do funkcji w zmiennej
 */
console.warn('referencja do funkcji w zmiennej');
var myFn = function () {
    console.log('myFn');
};
myFn();


/**
 * funkcja zwracająca inną funkcję
 * @returns {Function}
 */
console.warn('funkcja zwracająca inną funkcję');
function myFuncGenerator() {
    return function () {
        console.log("treść zwracanej funkcji anominowej");
    }
}
myFuncGenerator()();


/**
 * przekazywanie funkcji
 */
console.warn('przekazywanie funkcji');
function myChore() {
    console.log("robota");
}
function myStuff() {
    console.log('moja rzecz');
}
function doEveningChores(chores) {
    for (var i = 0; i < chores.length; i++) {
        chores[i]();
    }
}
doEveningChores([myChore, myStuff]);