"use strict";

/**
 * zmienna globalna - antywzorzec
 */
var animal = "Gorilla";


/**
 * Dostęp do zmiennej zakresu - funkcja haveAccessToAnimal i zmienna animal są w tym samym zakresie.
 */
console.warn('dostęp do zmiennej spoza funkcji');
function haveAccessToAnimal() {
    return animal;
}
console.log(haveAccessToAnimal());


/**
 * prywatna/lokalna zmienna funkcji
 * @returns {string}
 */
console.warn('zmienna lokalna funkcji');
function privateVar() {
    var innerVar = "zmienna lokalna funkcji privateVar";
    return innerVar;
}
console.log(privateVar()); // zmienna lokalna funkcji privateVar
try {
    console.log(innerVar);
} catch (e) {
    console.warn(e);
}


/**
 * wewnątrz funkcji pierszeństwo mają (jeżeli są zdefiniowane) zmienne lokalne
 */
console.warn('nadpisywanie wartości zmiennej');
function accessPriority() {
    var animal = "Dog";
    console.log(animal);
    console.log(window.animal);
}
accessPriority();
console.log(animal);


/**
 * blok pętli nie tworzy własnego zakresu/scope.
 */
console.warn('blok pętli nie tworzy własnego zakresu.');
for (var i = 0; i < 1; i++) {
    var animal = "Cat";
    console.log(animal);
    console.log(window.animal);
}
console.log(animal); // Cat


/**
 * hoisting.
 * Zadeklarowano zmienną "innerVar" w funkcji.
 * Interpreter JS przed wykonaniem kodu mapuje zmienne.
 * Wartoście zmiennych są przypisywane wg kolejności
 * dlatego wyświetlenie wartości przed przypisaniem wyświetla undefined a nie ReferenceError.
 */
console.warn('hoisting');
(function () {
    console.log(innerVar); // undefined
    var innerVar = 10;
    console.log(innerVar); // 10
})();


/**
 * kontekstem metody obiektu jest obiekt w którym się znajduje ta funkcja
 * Do właściwości obiektu odwołujemy się przez "this".
 */
console.warn('kontekst obiektu');
var obj = {
    someVal: "foo",
    say: function () {
        return this.someVal;
    }
};
console.log(obj.say());