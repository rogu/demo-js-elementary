/**
 * cudzysłowy - zagnieżdżenia
 */
var str1 = "Volvo XC60";
var str2 = 'Volvo XC60';

var str3 = "It's alright";
var str4 = "He is called 'Johnny'";
var str5 = 'He is called "Johnny"';

var myParagraph = "<p class='text-success'></p>";
var myDiv = '<div id="menu"></div>';
var mySpan = "<span id=\"menu\"></span>"; // znak ucieczki "\"


/**
 * metoda zamienia znak "-" na camelCase. np. well-known zamieni na wellKnown.
 * @param str {string} - wyrażenie filtrowane
 * @returns {string}
 */
function dashToCamelCase(str) {
    return str.replace(/-[a-z]/g, function (val) {
        return val.charAt(1).toUpperCase();
    });
}
console.warn('metoda replace');
var msg = dashToCamelCase('John is not well-knows but he is hard-working');
console.log(msg); // John is not wellKnows but he is hardWorking

/**
 * metoda zastępuje zmienne %s wartościami z tablicy.
 */

function sprintf(template, values) {
    return template.replace(/%s/g, function () {
        return values.shift();
    });
}

var interpolatedText = sprintf('The quick %s %s jumps over the lazy %s', [
    'brown',
    'fox',
    'dog'
]);

console.log(interpolatedText);


/**
 * metoda wyszukuje ciąg w ciągu; Jeżeli znajdzie ciąg zwraca jego index, w przeciwnym wypadku -1
 */
console.warn('metoda indexOf');
var result = "jakiś tekst".indexOf('tekst');
console.log(result); // 6


/**
 * metoda zwraca stringa przyciętego do indexu przekaznego w parametrze
 */
console.warn('metoda substring');
var str = "#home".substring(1);
console.log(str); // home


/**
 * metoda zwraca łańcuch znaków reprezentujący dany obiekt.
 */
console.warn('metoda toString');
var obj = {};
console.log(obj.toString()); // "[object Object]"
console.log([1, 2, 3, {id: 1}].toString()); // "1,2,3,[object Object]"
console.log(typeof [1, 2, 3, 4].toString()); // string


/**
 * metoda split przetwarza stringa na tablicę
 */
var ta = document.querySelector('#ta');
var btn = document.querySelector('#btn');
var list = document.querySelector('.list');

btn.addEventListener('click', function (evt) {
    if (!ta.value) {
        alert('dodaj tekst');
        return;
    }
    var arr = ta.value.split(/\n/g);

    arr.forEach(function (item) {
        var li = document.createElement('li');
        li.innerText = item;
        list.appendChild(li);
    });
});