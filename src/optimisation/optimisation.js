/**
 * wydajnośc js - poniższe rozwiązania są szybsze
 */

/**
 * pętle - buforowanie długości tablicy w zmiennej
 */
var myArray = [1, 2, 3];
var myLength = myArray.length; // przypisanie długości tablicy do zmiennej
for (var i = 0; i < myLength; i++) {
    // kod
}


/**
 * zmienne - jedna deklaracja, wiele zmiennych
 */
var a = 2,
    s = 'tekst',
    x,
    y;


/**
 * warunki - regex jest szybszy
 */
if (/^(foo|bar)$/.test('foo')) { // zamiast: if (type == 'foo' || type == 'bar') {
    // kod
}


/**
 * często używania właściwość lepiej przekazać do zmiennej
 */
var windowWidth = window.innerWidth;


/**
 * Literały są szybsze
 */
var tablica = [],
    obiekt = {};


/**
 * Rzutowanie do boolean
 */
console.warn('rzutowanie');
var test = 1;
console.log(!!test); // true
console.log(!!{}); // true


/**
 * Rzutowanie do string
 */
console.log(typeof ("" + 20)); // string


/**
 * Rzutowanie do number
 */
console.log(+true); // 1
console.log(typeof (+"10")); // number


/**
 * tylda (operator bitowy!) - umieszczona przed liczbą n daje -(n+1).
 */
console.warn('operator tylda');
console.log(~1); // -2
console.log(~2); // -3
console.log(~-1); // 0
// zaokrąglanie
console.log(~~2.555); // 2

var result = "jakiś tekst".indexOf('xyz');
if (!~result) console.log('tekst nie znaleziony');

var result2 = "jakiś tekst".indexOf('tekst');
if (~result2) console.log('tekst znaleziony');


/**
 * skrócony if
 */
console.warn('skrócony if');
var val = true;
val && console.log('ok'); // ok
