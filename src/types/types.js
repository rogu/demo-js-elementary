
/**
 * Data Types
 */
var length = 10; // Number
var lastName = "John"; // String
var cars = ["Saab", "Volvo", "BMW"]; // Array
var user = {firstName: "John", lastName: "Doe"}; // Object
var access = true; // Boolean
var value; // undefined


/**
 * Javascript należy do języków programowania które są dynamicznie typowane
 */
var x;      // undefined
x = 5;      // teraz to jest typ Number
x = "John"; // teraz to jest typ String

console.warn('Data Types');
console.log(typeof x); // string

var x1 = 34.00;      // liczba zmiennoprzecinkowa typu float
var x2 = 34;         // liczba całkowita typu integer
console.log(typeof x1); // number
console.log(typeof x2); // number


/**
 * undefined; w JS zadeklarowana zmienna bez wartości ma wartość "undefined"
 */
var neverDefined;
console.log((typeof neverDefined === "undefined")); // true
console.log((typeof neverDefined === typeof undefined)); // true


/**
 * null
 */
var str = 'Hej!';
console.log(str); // Hej!
str = null;
console.log(str); // null
console.log((typeof str == typeof null)); // true