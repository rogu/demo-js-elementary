// wykonanie z opóźnieniem
setTimeout(function () {
    console.log('settimeout')
}, 5000);


// wykonywanie w określonym interwale
var count = 0;
var myInterval = setInterval(function () {
    if (count >= 10) {
        clearInterval(myInterval);
    }
    console.log('count', count++);
}, 1000);