var value = 1.4;
var random = Math.random();

// wartość z przedziału 0-1
console.warn('losowa wartość');
console.log(random);

console.warn('zaokrąglanie');

// cail - zaokrągla w górę
console.log(Math.ceil(value)); // 2

// round - zaorkągla w dół od .4 i w górę od .5
console.log(Math.round(value)); // 1
console.log(Math.round(random)); // 0 albo 1

console.warn('min / max');
// min - zwraca najmniejszą wartość
var min = Math.min(0, 150, 30, 20, -8, -200);
console.log(min);

// max - zwraca największą wartość
var max = Math.max(0, 150, 30, 20, -8, -200);
console.log(max);
