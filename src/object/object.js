/**
 * Literał obiektu
 * this - wskazuje na własny kontekst.
 */
console.warn('obiekt literałowy');
var dog = {
    name: 'brutus',
    sound: 'hau!',
    say: function () {
        return this.name + ' say ' + this.sound;
    }
};
console.log(dog.say());


/**
 * hasOwnProperty zwraca true dla właściwości które są w "this"
 */
console.warn('iterowanie po właściwościach obiektu' );
for (var prop in dog) {
    if (dog.hasOwnProperty(prop)) {
        console.log('is own', prop);
    } else {
        console.log('is not own', prop);
    }
}


/**
 * Każdy obiekt dziedziczy prototyp globalnego obiektu "Object".
 * Dodając metodę do globalnego prototypu Obiektu sprawiasz że bedzie ona dostępna w każdym obiekcie
 */
console.warn('prototyp obiektu');
Object.prototype.dodaj = function (a, b) {
    return a + b;
};
console.log({}.dodaj(2, 2));
console.log(dog.dodaj(3, 3));


/**
 * Object getter and setter
 */

var greating = {
    value: "Hi!",
    get ello() {
        return this.value;
    },
    set ello(value) {
        this.value = value;
    }
};

console.log(greating.ello);
greating.ello = 'Welcome';
console.log(greating.ello);
