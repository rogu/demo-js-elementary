"use strict";

/**
 * funkcja test - sprawdza czy wzorzec został znaleziony w łańcuchu. Zwraca boolean
 */
console.warn('text method');
var str1 = 'masło maślane';
var example1 = /^mas/.test(str1); // ^ - początek wzorca
console.log(example1); // true

var example2 = /ane$/.test(str1); // $ - koniec wzorca
console.log(example2); // true

// dopasowanie: początek, koniec oraz słowa w środku zdania
var str2 = "Start with a good word and end with a kind deed";
console.log(/^Start (?=.*good)(?=.*kind).* deed$/.test(str2)); // true


// sprawdzenie maila
var example4 = /^\w+@\w+\.\w{2,3}(\.[a-z]{2})?$/;
console.log('email', example4.test("john@doe.com")); // true


/**
 * search - zwraca index pierwszego wystąpienia podciągu w ciągu:
 */
console.warn('search method');
var tekst = "Nic, panie kierowniku! Oczko mu się odlepiło. Temu misiu.";
console.log(tekst.search(/mu/gi)); // 29
// search działa podobnie do metody indexOf.
console.log(tekst.indexOf('mu')); // 29


/**
 * match - zwraca tablicę pasujących elementów
 */
console.warn('match method');
var str3 = "obj1 obj2 obj3 obj4";
var wzor = /obj[1-2]/g;
console.log(str3.match(wzor)); // ["obj1", "obj2"]

// wyodrębnienie z linku nazwy dokumentu HTML.
var hash = window.location.href.match(/[a-z-_0-9]+(?=\.html)/); // ?= - oznacza spojrzenie w przód
console.log('location (tylko nazwa pliku bez rozszerzenia html):', hash[0]);

/*
    sprawdzenie hasła - wymagane: małe i duże litry oraz cyfry; od 4 do 8 znaków
 */
(/^(?=.*\d)(?=.*\w).{4,8}$/).test("1a1B2c3y");
