/**
 * Callback - czyli wywołanie zwrotne funckcji
 */

/**
 * funkcja getData przygotowuje dane (i nie robi nic innego)
 * następnie uruchamia funkcję "callback" (przekazaną w parametrze) która np. zmienia DOM.
 * funkcja getData nie wie co robi funkcja callback - jedynie ją wywołuje.
 */
function getData(callback) {
    // symuluje asynchroniczność
    setTimeout(function () {
        var preparedData = [1, 2, 3].join();
        callback(preparedData);
    }, 1000);
}

function renderView(data) {
    document.querySelector('.content').innerHTML = data;
}

// przekazuje funkcję w parametrze
getData(renderView);

// tutaj z funkcją anonimową
getData(function (data) {
    console.log(data);
});