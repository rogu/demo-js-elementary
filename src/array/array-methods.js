var numbers = new Array();

/**
 * medoda push dodaje element na końcu tablicy
 */
numbers.push(7);


/**
 * medoda unshift dodaje element na początku tablicy
 */
numbers.unshift(0);


/**
 * metoda join przetwarza tablicę w ciąg znaków
 * jako parametr przyjmuje separator
 */
console.warn('metoda join');
console.log(numbers.join('---'));


/**
 * Iterowanie po tablicy
 */
console.warn('metoda forEach');
var arr = [1, 2, 3];
arr.forEach(function (el, index) {
    console.log(index, el);
});


/**
 * metoda splice (generalnie) usuwa z tablicy elemnty - może również (przy okazji) dodawać elementy do tablicy
 * Pierwszy parametr to index początkowy; drugi to ilość usuwanych elementów
 * Metoda usuwa elementy z oryginalnej tablicy tzn. zmienia samą siebie
 * Metoda splice zwraca nową tablicę zawierającą usunięte elementy
 */
console.warn('metoda splice');
var people = ['John', 'Mike', 'Julia'];
var spliceResult = people.splice(1, 1);
console.log(people);
console.log(spliceResult);


/**
 * Metoda slice zwraca wycinek tablicy.
 * Pierwszy parametr wskazuje na index początkowy, drugi na końcowy.
 * Nie zmienia tablicy na której jest uruchomiona.
 * array.slice(startIndex, endIndex)
 */
console.warn('metoda slice');
var cars = ['Audi', 'BMW', 'Fiat'];
var sliceResult = cars.slice(1, 2);
console.log(cars);
console.log(sliceResult);


/**
 * concat
 */
console.warn('metoda concat');
var a = ["raz", "dwa"];
var b = ["trzy", "cztery"];
var c = ["pięć", "sześć"];
var d = a.concat(b, c);
console.log(d); // ["raz", "dwa", "trzy", "cztery", "pięć", "sześć"]


/**
 * sort
 */
console.warn('metoda sort');
var tablica = ['Marcin', 'Anna', 'Piotr', 'Grzegorz'];
tablica.sort();
console.log(tablica.join()); // Anna,Grzegorz,Marcin,Piotr

// tworzymy funkcję do segregacji liczb
function porownajLiczby(a, b) {
    return a - b
}

var tablicaLiczb = [100, 10, 25, 310, 1200, 400];

console.log('Bez sortowania: ' + tablicaLiczb.join());

tablicaLiczb.sort();
console.log('sortowanie domyślne:' + tablicaLiczb.join());

tablicaLiczb.sort(porownajLiczby);
console.log('sortowanie z funkcją porównującą:' + tablicaLiczb.join());


/**
 * every - zwraca true jeżeli warunek jest prawdziwy dla wszystkich elementów tablicy
 */
console.warn('metoda every');
var access = [true, true, true];
var result = access.every(function (item) {
    return item;
});
console.log('every access are:', result);


/**
 * indexOf - zwraca index odnalezionej wartości
 */
console.warn('metoda indexOf');
var myArray = [1, 2, 3, "last item"];
var resultIndexOf = myArray.indexOf("last item");
console.log(resultIndexOf);


/**
 * map - Tworzy nową tablicę.
 * Funkcja przekazana w parametrze przyjmuje elementy starej tablicy, przetwarza je i przekazuje do nowej tablicy.
 */
console.warn('metoda map');
var arrMap = [0.9191, 0.8383, 1.3333];
var mapResult = arrMap.map(function (item) {
    return +item.toFixed(2);
});
console.log(mapResult);


/**
 * reduce - redukuje np. wyszystkie elementy tablicy do jednej wartości
 */
console.warn('metoda reduce');
var arrReduce = [1, 2, 3, 4, 5];
var reduceResult = arrReduce.reduce(function (acc, nextItem) {
    return acc + nextItem;
}, 0);
console.log(reduceResult);


/**
 * filter - Tworzy nową tablicę z wszystkimi elementami, które przechodzą poprawnie zrealizowany w postaci dostarczonej funkcji test.
 */

console.warn('metoda filter');
var arrFilter = [0, 1, 2, 3, 4, 5];
var filteredArray = arrFilter.filter(function (value) {
    return value > 2;
});
console.log(filteredArray);
