/**
 * tablica (literał) indeksowana od 0.
 */
console.warn('sposób 1');
var cars = ["Saab", "Volvo", "BMW"];
// inny sposób na utworzenie talicy: new Array("Saab", "Volvo", "BMW")

console.log(cars[0]);
console.log(cars instanceof Array);


/**
 * tablica asocjacyjna
 */
console.warn('sposób 2');
var colors = [];
colors['red'] = "#ff0000";
colors['blue'] = "#0000ff";
colors.yellow = "#ffff00";

for (var key in colors) {
    console.log('color ' + key + ' is ' + colors[key]);
}
console.log(cars instanceof Array);


/**
 * tablica wielowymiarowa
 */
console.warn('sposób 3');
var people = [];
people[0] = ['Jan', 'Kowalski'];
people[1] = ['Michał', 'Iksinski'];
people.push(['John', 'Doe']);
console.log('name:', people[0][0], ', lastname:', people[0][1]);


/**
 * tablica obiektów
 */
console.warn('sposób 4');
var contacts = [
    {name: "Joe", phone: 939393939},
    {name: "Kris", phone: 2343223432}
];
console.log(contacts[0].name);
