var person = {
    firstName: "John",
    lastName: "Doe",
    age: 22
};

var car = {
    brand: "Audi",
    type: "sedan",
    engine: "oil",
    getParams: function (arg1, arg2, arg3) {
        console.log(this[arg1], this[arg2], this[arg3]);
    }
};

/**
 * apply - wykonuje się w kontekscie obiektu przekazanego w 1-szym parametrze(tutaj person);
 * apply wywołuje się od razu.
 * Argumenty, które chcemy przekazać do wykonywnej funkcji podajemy w tablicy.
 *
 * W poniższym przykładzie "pożyczam" metodę getParams z obiektu car i wykonuję ją w kontekscie obiektu person
 * w parametrze przekazuję tablicę.
 */
car.getParams.apply(person, ['firstName', 'lastName', 'age']);