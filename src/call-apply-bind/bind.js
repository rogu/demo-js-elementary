var person = {
    firstName: "John",
    lastName: "Doe",
    age: 22
};

var car = {
    brand: "Audi",
    type: "sedan",
    engine: "oil"
};

// funkcja getParam nie jest powiązana (jeszcze) z żadnym obiektem
function getParam(param) {
    return this[param];
}


/**
 * bind - podobnie jak call, z tą różnicą, że zwraca funkcję którą można wykonać później.
 * Tworzy nową funkcję której kontekstem jest obiekt przekazany do funkcji bind.
 */
var getParamFromPerson = getParam.bind(person);
console.log(getParamFromPerson('age'));

var getParamFromCar = getParam.bind(car);
console.log(getParamFromCar('engine'));