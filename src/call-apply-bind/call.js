var person = {
    firstName: "John",
    lastName: "Doe",
    age: 22
};

var car = {
    brand: "Audi",
    type: "sedan",
    engine: "oil"
};

// funkcja getParam nie jest powiązana (jeszcze) z obiektem myObject
function getParam(param) {
    return this[param];
}


/**
 * call - wykonuje się w kontekscie obiektu przekazanego w 1-szym parametrze(tutaj myObject); call wywołuje się od razu.
 * Argumenty, które chcemy przekazać do wykonywnej funkcji podajemy po przecinkach.
 */
var personName = getParam.call(person, 'firstName');
console.log(personName);

var carBrand = getParam.call(car, 'brand');
console.log(carBrand);