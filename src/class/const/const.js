/**
 * Właściwości statyczne
 */
var MyClass = function () {
    this.nonstatic = 'zmienna nie statyczna';
};
MyClass.STATIC = 'zmienna statyczna';
MyClass.PI = 3.14;

console.log(MyClass.STATIC); // "zmienna statyczna"
console.log(MyClass.PI); // 3.14
console.log(MyClass.nonstatic); // undefined


/**
 * Metoda statyczna
 */
function Helpers() {
}

Helpers.action = function (param) {
    alert('static class method ' + param);
};

/**
 * Nie trzeba tworzyć instancji żeby uruchomić statyczną metodę
 */
Helpers.action('works');