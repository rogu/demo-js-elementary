/**
 * To co zwraca konstuktor (return) jest publiczne.
 * W poniższym przykładzie:
 * metody publiczne: "getName" i "setName"
 * zmienna prywatna: "name"
 */
var Car = function (carName) {
    var name = carName; // private
    return { // public
        getName: function () {
            return name;
        },
        setName: function (newName) {
            name = newName;
        }
    };
};
var car = new Car('Fiat');
console.log(car.getName()); // Fiat
console.log(car.name); // undefined
car.setName('BMW');
console.log(car.getName()); // BMW
