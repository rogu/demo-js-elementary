/**
 * konstrukor Clothes z wywołaniem konstruktora Product
 */
function Clothes(name, price, size) {
    Product.call(this, name, price, 'clothes');
    this.size = size;
}

/**
 * dziedziczenie prototypowe
 */
Clothes.prototype = Object.create(Product.prototype);

/**
 * nadpisanie metody getParams w klasie Clothes
 */
Clothes.prototype.getParams = function () {
    var item = _.clone(this);
    item.price += ' EU';
    return item;
};

