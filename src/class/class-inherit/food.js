/**
 * konstrukor Food z wywołaniem konstruktora Product (super)
 */
function Food(name, price, weight) {
    Product.call(this, name, price, 'food');
    this.weight = weight;
}

/**
 * dziedziczenie prototypowe
 */
Food.prototype = Object.create(Product.prototype);