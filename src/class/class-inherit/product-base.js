/**
 * konstruktor przodka - klasa bazowa
 */

function Product(name, price, category) {
    console.warn('Product constructor');
    this.name = name;
    this.price = price;
    this.category = category;
}
Product.prototype.getParams = function () {
    return this;
};