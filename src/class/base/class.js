/**
 * konstruktor klasy
 */
var Person = function (name) {
    this.name = name;
    this.getName = function () {
        return this.name;
    }
};

// utworzenie instancji
var person1 = new Person('Joe');
var person2 = new Person('Mike');

console.log(person1.getName(), person2.getName());